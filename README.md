# react-native-brightness-controller

brightness-controller

## Installation

```sh
npm install react-native-brightness-controller
```

## Usage

```js
import BrightnessController from "react-native-brightness-controller";

// ...

const result = await BrightnessController.multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
