package com.reactnativebrightnesscontroller;

import android.app.Activity;
import android.provider.Settings;
import android.util.Log;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.module.annotations.ReactModule;

@ReactModule(name = BrightnessControllerModule.NAME)
public class BrightnessControllerModule extends ReactContextBaseJavaModule {
    public static final String NAME = "BrightnessController";

    public BrightnessControllerModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    @NonNull
    public String getName() {
        return NAME;
    }

    @ReactMethod
    public void setBrightnessLevel(final float brightnessLevel) {
      final Activity activity = getCurrentActivity();
      if (activity == null) {
        return;
      }

      activity.runOnUiThread(new Runnable() {
        @Override
        public void run() {
          WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
          lp.screenBrightness = brightnessLevel;
          activity.getWindow().setAttributes(lp);
        }
      });
    }
    @ReactMethod
    public void setDefaultSystemThemeMode() {
        final Activity activity = getCurrentActivity();
        if (activity == null) {
            return;
        }
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);;
            }
        });

    }

    @ReactMethod
    public void setDefaultThemeMode(@AppCompatDelegate.NightMode int mode) {
        final Activity activity = getCurrentActivity();
        if (activity == null) {
            return;
        }
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AppCompatDelegate.setDefaultNightMode(mode);
            }
        });
    }

    @ReactMethod
    public void getBrightnessLevel(Promise promise) {
      WindowManager.LayoutParams lp = getCurrentActivity().getWindow().getAttributes();
      promise.resolve(lp.screenBrightness);
    }

    @ReactMethod
    public void getSystemBrightnessLevel(Promise promise) {
      String brightness = Settings.System.getString(getCurrentActivity().getContentResolver(), "screen_brightness");
      promise.resolve(Integer.parseInt(brightness)/255f);
    }

    public static native int nativeMultiply(int a, int b);
}
