import * as React from 'react';

import { StyleSheet, View, Text } from 'react-native';
import BrightnessController from 'react-native-brightness-controller';

export default function App() {
  const [result, setResult] = React.useState<number | undefined>();

  React.useEffect(() => {
    BrightnessController.setBrightnessLevel(0);
    BrightnessController.getSystemBrightnessLevel().then((a) => console.log(a));
    BrightnessController.getBrightnessLevel().then((a) => {
      setResult(a);
      console.log(a);
    });
    let i = 0;
    const interval = setInterval(() => {
      BrightnessController.setBrightnessLevel(i);
      console.log(i);
      i = i + 0.1;
      if (i > 1) clearInterval(interval);
    }, 1000);
  }, []);

  return (
    <View style={styles.container}>
      <Text>F Result: {result}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: 60,
    height: 60,
    marginVertical: 20,
  },
});
