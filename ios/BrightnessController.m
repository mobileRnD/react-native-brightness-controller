#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(BrightnessController, NSObject)

RCT_EXTERN_METHOD(multiply:(float)a withB:(float)b
                 withResolver:(RCTPromiseResolveBlock)resolve
                 withRejecter:(RCTPromiseRejectBlock)reject)

RCT_REMAP_METHOD(getBrightnessLevel,
                 resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
    resolve(@([UIScreen mainScreen].brightness));
}
RCT_REMAP_METHOD(getSystemBrightnessLevel,
                 get_data_resolver:(RCTPromiseResolveBlock)resolve
                 get_data_rejecter:(RCTPromiseRejectBlock)reject)
{
    resolve(@([UIScreen mainScreen].brightness));
}
RCT_EXPORT_METHOD(setBrightnessLevel:(float)brightnessLevel)
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIScreen mainScreen].brightness = brightnessLevel;
    });
}

@end
