import UIKit

@objc(BrightnessController)
class BrightnessController: NSObject {

    @objc
    func getBrightnessLevel(_ resolve: RCTPromiseResolveBlock,
                            rejecter reject: RCTPromiseRejectBlock) {
        let currentBrightness = UIScreen.main.brightness
        resolve([currentBrightness])
    }

    @objc
    func getSystemBrightnessLevel(_ resolve: RCTPromiseResolveBlock,
                                  rejecter reject: RCTPromiseRejectBlock) {
        let currentBrightness = UIScreen.main.brightness
        resolve([currentBrightness])
    }

    @objc
    func setBrightnessLevel(_ brightnessLevel: Float) {
       UIScreen.main.brightness = CGFloat(brightnessLevel)
    }

    @objc(multiply:withB:withResolver:withRejecter:)
    func multiply(a: Float, b: Float, resolve:RCTPromiseResolveBlock,reject:RCTPromiseRejectBlock) -> Void {
        resolve(a*b)
    }
    @objc
    static func requiresMainQueueSetup() -> Bool {
    return true
    }
}
