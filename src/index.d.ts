declare type BrightnessControllerType = {
  setBrightnessLevel(a: number): Promise<void>;
  getSystemBrightnessLevel(): Promise<number>;
  getBrightnessLevel(): Promise<number>;
  /**
  * Only supported on android.
  *
  * @platform android
  */
  setDefaultSystemThemeMode(): void;
  /**
  * Only supported on android.
  *
  * @platform android
  */
   setDefaultThemeMode(mode: any): void;
};
declare let BrightnessControllerExport: BrightnessControllerType;
export default BrightnessControllerExport;
