import { NativeModules, Platform } from 'react-native';

type BrightnessControllerType = {
  setBrightnessLevel(a: number): Promise<void>;
  getSystemBrightnessLevel(): Promise<number>;
  getBrightnessLevel(): Promise<number>;
  /**
  * Only supported on android.
  *
  * @platform android
  */
  setDefaultSystemThemeMode(): void;
  /**
  * Only supported on android.
  *
  * @platform android
  */
  setDefaultThemeMode(mode: any): void;
};

const { BrightnessController } = NativeModules;

let BrightnessControllerExport: BrightnessControllerType;

if (Platform.OS === 'android') {
  const BrightnessControllerAndroid: BrightnessControllerType = {
    ...BrightnessController,
    setDefaultSystemThemeMode: () => {
      BrightnessController.setDefaultSystemThemeMode()
    },
    setDefaultThemeMode: (mode: any) => {
      BrightnessController.setDefaultThemeMode(mode)
    },
  };
  BrightnessControllerExport = BrightnessControllerAndroid;
} else {
  BrightnessControllerExport = {
    ...BrightnessController,
    setDefaultSystemThemeMode: () => {
      return;
    },
    setDefaultThemeMode: () => {
      return;
    },
  };
}

export default BrightnessControllerExport;
